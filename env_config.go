package env_config_go

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

type Error struct {
	code    int
	message string
	arg     string
}

func (e *Error) Error() string {
	if e.arg == "" {
		return fmt.Sprintf(e.message)
	} else {
		return fmt.Sprintf(e.message + "(" + e.arg + ")")
	}
}

var (
	ErrorEnvNotExits         = &Error{message: "environment not exist"}
	ErrorRegExpCompileFailed = &Error{message: "regular expression compile failed"}
)

func contains(v string, a []string) bool {
	for _, i := range a {
		if i == v {
			return true
		}
	}
	return false
}

func EnvConvertor(text []byte, lineComment []string) ([]byte, error) {
	var lineCommentRegEx []string

	if len(lineComment) == 0 {
		lineCommentRegEx = []string{"#"}
	} else {
		lineCommentRegEx = lineComment
	}

	regExFmt := "\\${[\\w\\s\"-/\\\\.:]*}"

	regEx := regExFmt
	for _, lineComment := range lineCommentRegEx {
		regEx = regEx + "|" + lineComment
	}

	compile, err := regexp.Compile(regEx)
	if err != nil {
		return nil, ErrorRegExpCompileFailed
	}

	cur := 0

	var output []byte
	for {
		matchData := compile.FindIndex(text[cur:])
		if matchData == nil {
			output = append(output, text[cur:]...)
			break
		}

		first := cur + matchData[0]
		last := cur + matchData[1]
		token := string(text[first:last])

		switch {
		case contains(token, lineCommentRegEx): // 라인 주석 처리
			idx := strings.Index(string(text[first:]), "\n")
			if idx < 0 {
				output = append(output, text[cur:]...)
				goto END
			}

			output = append(output, text[cur:first+idx /* \n */]...)

			cur += matchData[0] + idx
		default: // 환경 변수 설정
			output = append(output, text[cur:first]...)

			checkVal := token[2 : len(token)-1]
			var defaultVal *string
			var envCheckVal string
			defaultValIdx := strings.Index(checkVal, ":")
			if defaultValIdx != -1 {
				temp := checkVal[defaultValIdx+1:]
				defaultVal = &temp
				envCheckVal = checkVal[:defaultValIdx]
			} else {
				envCheckVal = checkVal
			}

			var envVal = os.Getenv(envCheckVal)
			if envVal == "" {
				if defaultVal != nil {
					envVal = *defaultVal
				} else {
					envNotExists := ErrorEnvNotExits
					envNotExists.arg = envCheckVal
					return nil, envNotExists
				}
			}

			output = append(output, []byte(envVal)...)

			cur += matchData[1]
		}
	}
END:

	return output, nil
}
