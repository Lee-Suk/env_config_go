package env_config_go

import (
	"fmt"
	"github.com/go-playground/assert/v2"
	"github.com/pelletier/go-toml"
	"os"
	"testing"
)

func TestUnmarshal(t *testing.T) {
	setEnv := func(key, val string) {
		err := os.Setenv(key, val)
		if err != nil {
			t.Error("env setting failed(" + err.Error() + ")")
			return
		}
	}
	setEnv("KEY1", "key1")
	setEnv("KEY2", "key2")
	setEnv("KEY3", "keykeykey")

	str := "[ENV.LIST]\n" +
		"PATH=${KEY1}${KEY2}\n" +
		"HOME=${KEY3}\n" +
		"USER=${KEY2}"

	output, err := EnvConvertor([]byte(str), nil)
	if err != nil {
		fmt.Println(err)
	}

	outputResult := "[ENV.LIST]\n" +
		"PATH=key1key2\n" +
		"HOME=keykeykey\n" +
		"USER=key2"

	if string(output) != outputResult {
		t.Error("Output mismatch")
	}
}
func TestUnmarshal2(t *testing.T) {
	setEnv := func(key, val string) {
		err := os.Setenv(key, val)
		if err != nil {
			t.Error("env setting failed(" + err.Error() + ")")
			return
		}
	}
	setEnv("KEY1", "key1")
	setEnv("KEY2", "key2")
	setEnv("KEY3", "keykeykey")

	str := "# ${EKY3}\n" +
		"[ENV.LIST]\n" +
		"# ${KEY1}\n" +
		"PATH=${KEY1}${KEY2}\n" +
		"HOME=${KEY3} # NEXT ${KEY2}\n" +
		"USER=${KEY2} # ${KEY2}"

	output, err := EnvConvertor([]byte(str), nil)
	if err != nil {
		fmt.Println(err)
	}

	outputResult := "# ${EKY3}\n" +
		"[ENV.LIST]\n" +
		"# ${KEY1}\n" +
		"PATH=key1key2\n" +
		"HOME=keykeykey # NEXT ${KEY2}\n" +
		"USER=key2 # ${KEY2}"

	if string(output) != outputResult {
		t.Error("Output mismatch")
	}
}

func TestUnmarshal3(t *testing.T) {
	setEnv := func(key, val string) {
		err := os.Setenv(key, val)
		if err != nil {
			t.Error("env setting failed(" + err.Error() + ")")
			return
		}
	}
	setEnv("KEY1", "key1")
	setEnv("KEY2", "key2")
	setEnv("KEY3", "keykeykey")

	str := "# ${EKY3}\n" +
		"[ENV.LIST]\n" +
		"# ${KEY1}\n" +
		"# PATH=${KEY1}${KEY2}\n" +
		"HOME=keykeykey # NEXT ${KEY2}\n" +
		"USER=${KEY2}"

	output, err := EnvConvertor([]byte(str), nil)
	if err != nil {
		fmt.Println(err)
	}

	outputResult := "# ${EKY3}\n" +
		"[ENV.LIST]\n" +
		"# ${KEY1}\n" +
		"# PATH=${KEY1}${KEY2}\n" +
		"HOME=keykeykey # NEXT ${KEY2}\n" +
		"USER=key2"

	if string(output) != outputResult {
		t.Error("Output mismatch")
	}
}

func TestUnmarshalDefaultKey(t *testing.T) {
	var str = `
		[ENV.LIST]
		PATH="${PATH}${HOME}"
		HOME="${UNKNOWN_TEST_FIELD:test}"
		USER="${USER}"
`

	output, err := EnvConvertor([]byte(str), nil)
	assert.Equal(t, err, nil)

	var data map[string]interface{}
	err = toml.Unmarshal(output, &data)
	assert.Equal(t, err, nil)

	assert.Equal(t, data["ENV"].(map[string]interface{})["LIST"].(map[string]interface{})["HOME"], "test")
}

func TestUnmarshalDefaultKeySpecial(t *testing.T) {
	var str = `
		[ENV.LIST]
		PATH="${PATH}${HOME}"
		HOME="${UNKNOWN_TEST_FIELD:-/\\.test}${VALUE_TEST:value}"
		USER="${USER}"
`

	output, err := EnvConvertor([]byte(str), nil)
	assert.Equal(t, err, nil)

	var data map[string]interface{}
	err = toml.Unmarshal(output, &data)
	assert.Equal(t, err, nil)

	assert.Equal(t, data["ENV"].(map[string]interface{})["LIST"].(map[string]interface{})["HOME"], "-/\\.testvalue")
}

func TestUnmarshalFieldFindError(t *testing.T) {
	var str = `
		[ENV.LIST]
		PATH="${PATH}${HOME}"
		HOME="${UNKNOWN_TEST_FIELD_ERROR}"
		USER="${USER}"
`

	output, err := EnvConvertor([]byte(str), nil)
	assert.Equal(t, err, ErrorEnvNotExits)

	assert.Equal(t, output, nil)
}

func TestUnmarshalFieldColon(t *testing.T) {
	var str = `
		[ENV.LIST]
		USER=${DEFAULT_TOKEN_TEST:":TEST"}
`

	output, err := EnvConvertor([]byte(str), nil)
	assert.Equal(t, err, nil)

	result := make(map[string]interface{})
	err = toml.Unmarshal(output, &result)
	assert.Equal(t, err, nil)
	assert.Equal(t, result["ENV"].(map[string]interface{})["LIST"].(map[string]interface{})["USER"], ":TEST")
}
