module gitlab.com/Lee-Suk/env_config_go

go 1.16

require (
	github.com/go-playground/assert/v2 v2.0.1
	github.com/pelletier/go-toml v1.9.4
)
